package vista;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import modelo.Alumno;

/**
 * Fichero: Vista.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-oct-2014
 */
public class Vista {

    public static boolean validarEmail(String s) {
        Pattern p = Pattern.compile("[\\w\\.]+@\\w+\\.\\w+");
        Matcher m = p.matcher(s);
        return m.matches();
    }

    public void readAlumno(Alumno alumno) {

        System.out.println("Id: " + alumno.getId());
        System.out.println("Nombre: " + alumno.getNombre());
        System.out.println("Edad: " + alumno.getEdad());
        System.out.println("Email: " + alumno.getEmail());

    }

    public char menuCrud() throws IOException {

        char opcion = ' ';

        System.out.println("MENU CRUD ");
        System.out.println("e. exit ");
        System.out.println("c. create ");
        System.out.println("r. read ");
        System.out.println("u. update ");
        System.out.println("d. delete ");
        System.out.print("Opción:  ");
        opcion = LeerCaracter();

        return opcion;
    }

    private char LeerCaracter() throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        char c = ' ';
        c = (char) isr.read();
        return c;
    }

    public void exit() {
        System.out.println("Fin ");
    }

    public String getId() throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        char c = ' ';
        String linea = "";

        System.out.print("Id: ");
        linea = br.readLine();

        return linea;

    }

    public char menuEstructura() throws IOException {

        char opcion = ' ';

        System.out.println("MENU ESTRUCTURA ");
        System.out.println("e. exit ");
        System.out.println("v. vector ");
        System.out.println("h. hashset ");
        System.out.println("f. fichero ");
        System.out.print("Opción:  ");
        opcion = LeerCaracter();

        return opcion;
    }

    public char menuModelo() throws IOException {

        char opcion = ' ';

        System.out.println("MENU MODELO ");
        System.out.println("e. exit ");
        System.out.println("a. alumno ");
        System.out.println("g. grupo ");
        System.out.print("Opción:  ");
        opcion = LeerCaracter();

        return opcion;

    }

    public void mostrarTexto(String s) {
        System.out.println(s);
    }

    public void mostrarError(String e) {
        System.err.println(e);
    }

}
