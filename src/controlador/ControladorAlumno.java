/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import modelo.Alumno;
import modelo.Grupo;
import modelo.IModelo;
import vista.IVista;
import vista.Vista;

/**
 * @author paco
 */
public class ControladorAlumno {

    IModelo modelo;
    IVista<Alumno> vistaAlumno;
    Alumno alumno = new Alumno();
    Grupo grupo = new Grupo();
    Vista vista = new Vista();

    ControladorAlumno(IModelo modelo, IVista<Alumno> vistaAlumno) throws IOException {
        this.modelo = modelo;
        this.vistaAlumno = vistaAlumno;
        menuCrud();
    }

    private void menuCrud() throws IOException {

        char opcion = ' ';
        do {
            vista.mostrarTexto("\nALUMNO");
            opcion = vista.menuCrud();
            switch (opcion) {
                case 'e': // Exit
                    vista.exit();
                    break;

                case 'c': // Create
                    crear();
                    break;

                case 'r': // Read alumno
                    reada();
                    break;

                case 'u':  // Actualizar
                    actualizar();
                    break;

                case 'd': // Borrar
                    borrar();
                    break;
                default:
                    vista.mostrarError("Opción Incorrecta");
                    break;
            }

        } while (opcion != 'e');

    }

    private void reada() {
        HashSet alumnos = modelo.reada();
        vistaAlumno.mostrar(alumnos);
    }

    private void crear() {
        Alumno alumno = null;
        Grupo grupo = null;

        alumno = vistaAlumno.obtener();
        grupo = getGrupo(alumno.getGrupo().getId());

        if (grupo != null) {
            alumno.setGrupo(grupo);
            modelo.create(alumno);
        } else {
            vista.mostrarError("Error. Grupo no encontrado");
        }
    }

    private void actualizar() throws IOException {

        String id = vista.getId();
        alumno = vistaAlumno.obtener();
        alumno.setId(id);
        grupo = getGrupo(alumno.getGrupo().getId());

        if (grupo != null) {
            alumno.setGrupo(grupo);
            modelo.update(alumno);
        } else {
            vista.mostrarError("Error. Grupo no encontrado");
        }
    }

    private void borrar() throws IOException {
        String id = vista.getId();
        alumno = new Alumno();
        alumno.setId(id);
        modelo.delete(alumno);
    }

    private Grupo getGrupo(String id) {
        Grupo encontrado = null;
        HashSet grupos = modelo.readg();
        Iterator it = grupos.iterator();
        while (it.hasNext()) {
            Grupo grupo = (Grupo) it.next();
            if (id.equals(grupo.getId())) {
                encontrado = grupo;
            }
        }
        return encontrado;

    }

}
