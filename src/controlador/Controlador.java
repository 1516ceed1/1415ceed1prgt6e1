package controlador;

import java.io.IOException;

import modelo.Alumno;
import modelo.Grupo;
import modelo.IModelo;
import modelo.ModeloFichero;
import modelo.ModeloHashSet;
import modelo.ModeloVector;
import vista.IVista;
import vista.Vista;
import vista.VistaAlumno;
import vista.VistaGrupo;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class Controlador {

    private IModelo modelo;
    private Vista vista;
    private char opcion;

    private IVista<Alumno> vistaAlumno;
    private IVista<Grupo> vistaGrupo;
    private ControladorAlumno calumno;
    private ControladorGrupo cgrupo;

    Controlador(IModelo modelo, Vista vista) throws IOException {

        this.modelo = modelo;
        this.vista = vista;

        opcion = menuEstructura();
        if (opcion != 'e') {
            menuModelo();
        }
    }

    private char menuEstructura() throws IOException {

        String linea;
        Boolean repetir = false;
        char opcion = ' ';

        do {
            opcion = vista.menuEstructura();
            switch (opcion) {
                case 'e': // Exit
                    vista.exit();
                    repetir = false;
                    break;
                case 'v':
                    modelo = new ModeloVector();
                    repetir = false;
                    break;
                case 'h':
                    modelo = new ModeloHashSet();
                    repetir = false;
                    break;
                case 'f':
                    modelo = new ModeloFichero();
                    repetir = false;
                    break;
                default:
                    vista.mostrarError("Opción Incorrecta");
                    repetir = true;
                    break;
            }
        } while (repetir == true);

        return opcion;

    }

    private char menuModelo() throws IOException {

        VistaAlumno valumno = new VistaAlumno();
        String linea;
        char opcionmodelo = ' ';
        Alumno alumno;

        //inicializaObjetos();
        do {

            switch (opcion) {
                case 'v': // Exit
                    vista.mostrarTexto("\nVECTOR");
                    break;
                case 'h': // Exit
                    vista.mostrarTexto("\nHASHSET");
                    break;
                case 'f': // Exit
                    vista.mostrarTexto("\nFICHERO");
                    break;
            }

            opcionmodelo = vista.menuModelo();
            switch (opcionmodelo) {
                case 'e': // Exit
                    vista.exit();
                    break;
                case 'a': // Alumno
                    vistaAlumno = new VistaAlumno();
                    calumno = new ControladorAlumno(modelo, vistaAlumno);
                    break;
                case 'g': // Grupos
                    vistaGrupo = new VistaGrupo();
                    ControladorGrupo cgrupo = new ControladorGrupo(modelo, vistaGrupo);
                    break;

                default:
                    vista.mostrarError("Opción Incorrecta");
                    break;
            }

        } while (opcionmodelo != 'e');
        return opcionmodelo;
    }

    private void inicializaObjetos() {

        Alumno alumno = new Alumno();
        Grupo grupo = new Grupo();

        grupo.setId("0");
        grupo.setNombre("Grupo0");

        alumno.setId("0");
        alumno.setNombre("Paco");
        alumno.setEdad(25);
        alumno.setEmail("paco.aldarias@ceedcv.es");

        alumno.setGrupo(grupo);

        modelo.create(alumno);
        modelo.create(grupo);

    }

}
