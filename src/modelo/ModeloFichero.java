package modelo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.StringTokenizer;

/**
 *
 * @author paco
 */
public class ModeloFichero implements IModelo {

    private File falumnos;
    private File fgrupos;
    private int ida = 0;
    private int idg = 0;
    private static String grupos = "grupos.cvs";
    private static String alumnos = "alumnos.cvs";

    private int calculaida() {
        int idmax = 0;

        if (falumnos.exists()) {

            try {

                FileReader fr = new FileReader(falumnos);
                BufferedReader br = new BufferedReader(fr);
                Alumno alumno;
                String linea;
                int id_;

                linea = br.readLine();
                while (linea != null) {
                    alumno = extraeAlumno(linea);
                    id_ = Integer.parseInt(alumno.getId());
                    if (id_ > idmax) {
                        idmax = id_;
                    }
                    linea = br.readLine();
                }

                fr.close();

            } catch (FileNotFoundException ex) {
            } catch (IOException ex) {
            }

        }
        return idmax;

    }

    private int calculaidg() {
        int idmax = 0;

        if (fgrupos.exists()) {

            try {

                FileReader fr = new FileReader(fgrupos);
                BufferedReader br = new BufferedReader(fr);
                Grupo grupo;
                String linea;
                int id_;

                linea = br.readLine();
                while (linea != null) {
                    grupo = extraeGrupo(linea);
                    id_ = Integer.parseInt(grupo.getId());
                    if (id_ > idmax) {
                        idmax = id_;
                    }
                    linea = br.readLine();
                }

                fr.close();

            } catch (FileNotFoundException ex) {
            } catch (IOException ex) {
            }

        }
        return idmax;

    }

    public ModeloFichero() throws IOException {
        falumnos = new File(alumnos);
        fgrupos = new File(grupos);
        ida = calculaida();
        idg = calculaidg();
    }

    @Override
    public void create(Alumno alumno) {
        ida++;
        alumno.setId(ida + "");
        FileWriter fw = null;
        try {
            fw = new FileWriter(falumnos, true);
            grabarAlumno(alumno, fw);
            fw.close();
        } catch (IOException ex) {
        }

    }

    @Override
    public void create(Grupo grupo) {
        idg++;
        grupo.setId(idg + "");
        FileWriter fw = null;
        try {
            fw = new FileWriter(fgrupos, true);
            grabarGrupo(grupo, fw);
            fw.close();
        } catch (IOException ex) {
        }

    }

    private Alumno extraeAlumno(String linea) {

        Alumno alumno;
        StringTokenizer str = new StringTokenizer(linea, ";");

        String id = str.nextToken();
        String nombre = str.nextToken();
        String edad_ = str.nextToken();
        int edad = Integer.parseInt(edad_);
        String email = str.nextToken();
        String idgrupo = str.nextToken();

        alumno = new Alumno();
        alumno.setId(id);
        alumno.setNombre(nombre);
        alumno.setEdad(edad);
        alumno.setEmail(email);

        Grupo grupo = new Grupo();
        grupo.setId(idgrupo);

        alumno.setGrupo(grupo);

        return alumno;
    }

    private Grupo extraeGrupo(String linea) {

        Grupo grupo;
        StringTokenizer str = new StringTokenizer(linea, ";");

        String id = str.nextToken();
        String nombre = str.nextToken();

        grupo = new Grupo();
        grupo.setId(id);
        grupo.setNombre(nombre);
        return grupo;
    }

    @Override
    public HashSet reada() {
        HashSet alumnos = new HashSet();
        Grupo grupo;
        try {
            FileReader fr = new FileReader(falumnos);
            BufferedReader br = new BufferedReader(fr);
            Alumno alumno;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                alumno = extraeAlumno(linea);
                grupo = getGrupo(alumno.getGrupo().getId());
                alumno.setGrupo(grupo);
                alumnos.add(alumno);
                linea = br.readLine();
            }
            fr.close();
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
        return alumnos;
    }

    @Override
    public void update(Alumno alumno) {

        try {
            File temp = new File("temp.txt");
            FileWriter fw = new FileWriter(temp);
            FileReader fr = new FileReader(falumnos);

            BufferedReader br = new BufferedReader(fr);
            Alumno a;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                a = extraeAlumno(linea);
                if (a.getId().equals(alumno.getId())) {
                    grabarAlumno(alumno, fw);
                } else {
                    grabarAlumno(a, fw);
                }
                linea = br.readLine();
            }
            fr.close();
            fw.close();

            falumnos.delete();
            temp.renameTo(falumnos);

        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }

    }

    @Override
    public void delete(Alumno alumno) {

        try {

            File temp = new File("temp.txt");

            FileWriter fw = new FileWriter(temp);
            FileReader fr = new FileReader(falumnos);

            BufferedReader br = new BufferedReader(fr);
            Alumno a;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                a = extraeAlumno(linea);
                if (!a.getId().equals(alumno.getId())) {
                    grabarAlumno(a, fw);
                }
                linea = br.readLine();
            }
            fr.close();
            fw.close();

            falumnos.delete();
            temp.renameTo(falumnos);

        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }

    }

    private void grabarAlumno(Alumno alumno, FileWriter fw) throws IOException {
        fw.write(alumno.getId());
        fw.write(";");
        fw.write(alumno.getNombre());
        fw.write(";");
        fw.write(alumno.getEdad() + "");
        fw.write(";");
        fw.write(alumno.getEmail());
        fw.write(";");
        fw.write(alumno.getGrupo().getId());
        fw.write("\r\n");
    }

    private void grabarGrupo(Grupo grupo, FileWriter fw) throws IOException {
        fw.write(grupo.getId());
        fw.write(";");
        fw.write(grupo.getNombre());
        fw.write("\r\n");
    }

    @Override
    public void update(Grupo grupo) {
        try {
            File temp = new File("temp.txt");
            FileWriter fw = new FileWriter(temp);
            FileReader fr = new FileReader(fgrupos);

            BufferedReader br = new BufferedReader(fr);
            Alumno a;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                a = extraeAlumno(linea);
                if (a.getId().equals(grupo.getId())) {
                    grabarGrupo(grupo, fw);
                } else {
                    grabarGrupo(grupo, fw);
                }
                linea = br.readLine();
            }
            fr.close();
            fw.close();

            fgrupos.delete();
            temp.renameTo(fgrupos);

        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
    }

    @Override
    public void delete(Grupo grupo) {
        try {

            File temp = new File("temp.txt");

            FileWriter fw = new FileWriter(temp);
            FileReader fr = new FileReader(fgrupos);

            BufferedReader br = new BufferedReader(fr);
            Grupo g;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                g = extraeGrupo(linea);
                if (!g.getId().equals(grupo.getId())) {
                    grabarGrupo(grupo, fw);
                }
                linea = br.readLine();
            }
            fr.close();
            fw.close();

            fgrupos.delete();
            temp.renameTo(fgrupos);

        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
    }

    @Override
    public HashSet<Grupo> readg() {
        HashSet grupos = new HashSet();
        try {
            FileReader fr = new FileReader(fgrupos);
            BufferedReader br = new BufferedReader(fr);
            Grupo grupo;
            String linea;

            linea = br.readLine();
            while (linea != null) {
                grupo = extraeGrupo(linea);
                grupos.add(grupo);
                linea = br.readLine();
            }
            fr.close();
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
        return grupos;
    }

    private Grupo getGrupo(String id) {

        Grupo encontrado = null;
        HashSet grupos = readg();
        Iterator it = grupos.iterator();
        while (it.hasNext()) {
            Grupo grupo = (Grupo) it.next();
            if (id.equals(grupo.getId())) {
                encontrado = grupo;
            }
        }
        return encontrado;

    }

}
